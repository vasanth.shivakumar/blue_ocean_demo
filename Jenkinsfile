pipeline {
    agent none
    options {
        gitLabConnection('GITLABAPI')
        gitlabBuilds(builds: ['build', 'test1', 'test2', 'test3', 'publish', 'archive'])
        buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '5'))
        preserveStashes(buildCount: 3)
    }
    stages {
        stage('Build') {
            agent {
                dockerfile {
                    filename 'Dockerfile'
                }
            }        
            steps {
                echo 'Starting build'
                checkout scm
                sh  '''echo "WORKSPACE: `pwd`"''' 
                sh  '''make clean'''
                sh  '''make prepare'''
                sh  '''make -f ${WORKSPACE}/Makefile -C ${WORKSPACE}/doxygen/build build'''
                stash includes: 'doxygen/build/bin/doxygen', name: 'doxygen'
                sh  '''make clean_build'''
                stash includes: 'doxygen/**', name: 'doxygen_src'                
                sh  '''make clean'''
            }        
            post {
                failure {
                    updateGitlabCommitStatus(name: 'build', state: 'failed')
                }
                success {
                    updateGitlabCommitStatus(name: 'build', state: 'success')
                }
            }
        }
        stage('Test') {
            parallel {
                stage('Test1') {
                    agent {
                        dockerfile {
                            filename 'Dockerfile'
                        }
                    }                 
                    steps {
                        echo 'Starting test1'
                        checkout scm
                        unstash 'doxygen_src'
                        unstash 'doxygen'
                        sh  '''echo "WORKSPACE: `pwd`"'''
                        sh  '''make test1'''
                    }        
                    post {
                        failure {
                            updateGitlabCommitStatus(name: 'test1', state: 'failed')
                        }
                        success {
                            updateGitlabCommitStatus(name: 'test1', state: 'success')
                        }
                    }
                }
                stage('Test2') {
                    agent {
                        dockerfile {
                            filename 'Dockerfile'
                        }
                    }                
                    steps {
                        echo 'Starting test2'
                        checkout scm
                        unstash 'doxygen_src'
                        unstash 'doxygen'
                        sh  '''echo "WORKSPACE: `pwd`"'''
                        sh  '''make test2'''
                    }        
                    post {
                        failure {
                            updateGitlabCommitStatus(name: 'test2', state: 'failed')
                        }
                        success {
                            updateGitlabCommitStatus(name: 'test2', state: 'success')
                        }
                    }
                }
                stage('Test3') {
                    agent {
                        dockerfile {
                            filename 'Dockerfile'
                        }
                    }                
                    steps {
                        echo 'Starting test3'
                        checkout scm
                        unstash 'doxygen_src'
                        unstash 'doxygen'
                        sh  '''echo "WORKSPACE: `pwd`"'''
                        sh  '''make -f ${WORKSPACE}/Makefile -C ${WORKSPACE}/doxygen test3'''
                        stash includes: 'doxygen/doxygen_docs/html/**', name: 'doxygen_html'
                    }        
                    post {
                        failure {
                            updateGitlabCommitStatus(name: 'test3', state: 'failed')
                        }
                        success {
                            updateGitlabCommitStatus(name: 'test3', state: 'success')
                        }
                    }
                }                
            }
        }
        stage('Publish') {
            agent {
                dockerfile {
                filename 'Dockerfile'
                }
            }                
            steps {
                echo 'Starting Publish'
                checkout scm
                unstash 'doxygen_html'
                publishHTML([allowMissing: true, alwaysLinkToLastBuild: true, keepAll: false, reportDir: 'doxygen/doxygen_docs/html', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])
            }        
            post {
                failure {
                    updateGitlabCommitStatus(name: 'publish', state: 'failed')
                }
                success {
                    updateGitlabCommitStatus(name: 'publish', state: 'success')
                }
            }
        }        
        stage('Archive') {
            agent {
                dockerfile {
                filename 'Dockerfile'
                }
            }                
            steps {
                echo 'Starting Archive'
                checkout scm
                archiveArtifacts allowEmptyArchive: true, artifacts: 'Dockerfile, Jenkinsfile', fingerprint: true
            }        
            post {
                failure {
                    updateGitlabCommitStatus(name: 'archive', state: 'failed')
                }
                success {
                    updateGitlabCommitStatus(name: 'archive', state: 'success')
                }
            }
        }        
    }
}