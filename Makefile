export PATH := ${PATH}:${WORKSPACE}/doxygen/build/bin:.
RM      = rm -rf
MKDIR   = mkdir -p
default: all

all: prepare build test1 test2 test3

prepare:
	git clone https://github.com/doxygen/doxygen.git
	$(RM) ${WORKSPACE}/doxygen/.git
	${MKDIR} ${WORKSPACE}/doxygen/build

build: ${WORKSPACE}/doxygen/build
	cmake -G "Unix Makefiles" ..
	make

test1: ${WORKSPACE}/doxygen/build/bin/doxygen
	doxygen -v

test2: ${WORKSPACE}/doxygen/build/bin/doxygen
	doxygen -h
        
test3: ${WORKSPACE}/doxygen/build/bin/doxygen ${WORKSPACE}/doxygen
	doxygen

clean:
	$(RM) ${WORKSPACE}/doxygen

clean_build:
	$(RM) ${WORKSPACE}/doxygen/build	