FROM ubuntu:16.04

LABEL maintainer="Vasanth Shivakumar vasanth.shivakumar@gmail.com"

RUN apt-get -y update --fix-missing
RUN apt-get -y install make cmake gcc g++ python cmake flex bison git graphviz
